FROM registry.gitlab.com/sharplinestech/sl_tools_core:latest

RUN apk add --no-cache linux-headers automake autoconf libtool

RUN mkdir /usr/sl_web_src && \
    mkdir /usr/sl_web_bld

#Gather deps is a separate copy so that it gets cached
COPY ./gather_deps.sh /usr/sl_web_src/gather_deps.sh
RUN /usr/sl_web_src/gather_deps.sh /usr/local

COPY . /usr/sl_web_src

WORKDIR /usr/sl_web_bld

RUN mkdir sl_tools_web && cd sl_tools_web && \
    cmake -DCMAKE_BUILD_TYPE=RELEASE -DCMAKE_INSTALL_PREFIX=/usr/local /usr/sl_web_src && \
    make -j $(nproc) && make install

WORKDIR /home

RUN rm -rf /usr/sl_web_src && \
    rm -rf /usr/sl_web_bld
