#!/bin/sh

ASIO_VERSION=asio-1-12-2
CMARK_VERSION=0.28.3
LIBSASS_VERSION=3.5.5

REF_PATH=$(pwd)
DST_PATH=$1
BUILD_TYPE=RELEASE



git clone --depth=1 --single-branch --branch $ASIO_VERSION https://github.com/chriskohlhoff/asio.git
mkdir $DST_PATH/include
cp -R asio/asio/include/asio $DST_PATH/include/asio
cp -R asio/asio/include/asio.hpp $DST_PATH/include/asio.hpp

cd $REF_PATH
rm -rf asio

git clone --depth=1 https://github.com/uriparser/uriparser.git
mkdir uriparser_bld
cd uriparser_bld
cmake -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DBUILD_SHARED_LIBS=OFF -DURIPARSER_BUILD_DOCS=OFF -DURIPARSER_BUILD_TESTS=OFF -DURIPARSER_BUILD_TOOLS=OFF -DURIPARSER_BUILD_WCHAR_T=OFF -DCMAKE_INSTALL_PREFIX=$DST_PATH ../uriparser
make -j $(nproc) && make install
cd $REF_PATH
rm -rf uriparser
rm -rf uriparser_bld


git clone --depth=1 --single-branch --branch $CMARK_VERSION https://github.com/commonmark/cmark.git
mkdir cmark_bld
cd cmark_bld
cmake -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DCMAKE_INSTALL_PREFIX=$DST_PATH -DCMARK_TESTS=OFF ../cmark
make -j $(nproc) && make install
cd $REF_PATH
rm -rf cmark
rm -rf cmark_bld

git clone --depth=1 --single-branch --branch $LIBSASS_VERSION https://github.com/sass/libsass.git
cd libsass
autoreconf --force --install
./configure --disable-tests --disable-shared --prefix=$DST_PATH
make -j $(nproc) 
make install
cd $REF_PATH
rm -rf libsass

git clone --depth=1 --single-branch --branch v0.10.1 https://github.com/google/gumbo-parser.git
cd gumbo-parser
./autogen.sh
./configure --prefix=$DST_PATH
make
make install
cd $REF_PATH
rm -rf gumbo-parser