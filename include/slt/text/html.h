#ifndef SLT_TEXT_HTML_INCLUDED_H
#define SLT_TEXT_HTML_INCLUDED_H

#include <string>
#include <iostream>

namespace slt {
  void minimize_html(std::string_view raw, std::ostream& stream);
}

#endif