#ifndef SLT_TEXT_MARKDOWN_INCLUDED_H
#define SLT_TEXT_MARKDOWN_INCLUDED_H

#include <iostream>

namespace slt {
  void cmark_to_html(std::string_view raw, int options, std::ostream& stream);
}

#endif