#ifndef SLT_TEXT_SASS_INCLUDED_H
#define SLT_TEXT_SASS_INCLUDED_H

#include <iostream>

namespace slt {
  void sass_to_css(std::string_view raw, std::ostream& stream);
}

#endif