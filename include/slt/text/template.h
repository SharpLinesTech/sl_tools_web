#ifndef SLT_TEXT_TEMPLATE_H
#define SLT_TEXT_TEMPLATE_H

#include "slt/text/template/op.h"
#include "slt/text/template/context.h"

#include "slt/mem/data_block.h"
#include "slt/concur/future.h"

#include <memory>
#include <string>
#include <future>

namespace slt {
  namespace text {

  class Template;

  // A template that is interpreted on demand
  class Template {
  public:
     Template() = default;
     Template(slt::Data_block data, std::vector<std::unique_ptr<tmpl::Op>> ops);
     Template(Template&&) = default;

     Template(const Template&) = delete;
     ~Template();

     Template& operator=(Template&& rhs) = default;
     // Renders the template to the destination stream
     void render(std::ostream& dst, const tmpl::Context_api& ctx) const;

   private:
     slt::Data_block raw_data;
     std::vector<std::unique_ptr<tmpl::Op>> operations_;
  };

  using Template_lookup_fn = std::function<slt::Shared_future<Template>(std::string_view)>;

  slt::Future<Template> load_template(slt::Data_block data, Template_lookup_fn fn);
}
}  // namespace cpl_tmpl
#endif