#ifndef SLT_TEXT_TEMPLATE_AST_H
#define SLT_TEXT_TEMPLATE_AST_H

#include <iostream>
#include <memory>

#include "slt/text/template/data_provider.h"

namespace slt {
namespace text {

namespace tmpl {
class Context_api;
class Expr {
 public:
  virtual ~Expr() {}
  virtual Data_provider eval(const Context_api& ctx) const = 0;
};

class String_literal final : public Expr {
 public:
  String_literal(std::string_view raw_data);
  Data_provider eval(const Context_api& ctx) const override;

  std::string data_;
};

// Identifier
class Id_ref final : public Expr {
 public:
  Id_ref(std::string_view);

  Data_provider eval(const Context_api& ctx) const override;

 private:
  std::string_view id_;
};

// <Expr>.Identifier
class Resolve final : public Expr {
 public:
  Resolve(std::unique_ptr<Expr>, std::string_view);
  Data_provider eval(const Context_api& ctx) const override;

 private:
  std::unique_ptr<Expr> lhs_;
  std::string_view key_;
};

// <Expr> |Identifier
class Filter final : public Expr {
 public:
  Filter(std::unique_ptr<Expr>, std::string_view);
  
  Data_provider eval(const Context_api& ctx) const override;

private:
  std::unique_ptr<Expr> lhs_;
  std::string_view filter_name_;
};
}  // namespace tmpl
}  // namespace text
}  // namespace slt

#endif