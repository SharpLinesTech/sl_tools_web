#ifndef SLT_TEXT_TEMPLATE_CONTEXT_H
#define SLT_TEXT_TEMPLATE_CONTEXT_H

#include <optional>

namespace slt {
namespace text {
namespace tmpl {

class Block_op;
class Context_api {
public:
  virtual ~Context_api() {}
  virtual std::optional<Data_provider> lookup(
      const std::string_view& name) const = 0;

  virtual const Block_op* lookup_block(const std::string_view& name) const {
    return nullptr;
  }
};

class Context : public Context_api {
 public:
  Context() = default;
  Context(const Context_api* parent) : parent_(parent) {}

  std::optional<Data_provider> lookup(
      const std::string_view& name) const override {
    auto found = data_roots_.find(name);
    if (found == data_roots_.end()) {
      if(parent_) {
        return parent_->lookup(name);
      }

      return std::nullopt;
    }

    return found->second;
  }

  Data_provider& add_root(std::string_view key) {
    return data_roots_[key];
  }

  template <typename T>
  Data_provider& add_root(std::string_view key, const T& data) {
    Data_provider& result = data_roots_[key];
    result = data_provider(data);
    return result; 
  }

  Data_provider& add_root_provider(std::string_view key, Data_provider data) {
    Data_provider& result = data_roots_[key];
    result = std::move(data);
    return result; 
  }

 private:
  std::unordered_map<std::string_view, Data_provider> data_roots_;
  const Context_api* parent_ = nullptr;
};
}  // namespace tmpl
}  // namespace text
}  // namespace slt

#endif