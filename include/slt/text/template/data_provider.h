#ifndef SLT_TEXT_TEMPLATE_DATA_PROVIDER_H
#define SLT_TEXT_TEMPLATE_DATA_PROVIDER_H

#include "slt/json.h"

#include <iostream>
#include <map>
#include <memory>
#include <functional>

namespace slt {
namespace text {
namespace tmpl {

template <typename T, typename Enable = void>
struct Data_traits {
  static constexpr bool is_sequence = false;
  static constexpr bool is_mapping = false;
  static constexpr bool is_value = true;

  static bool as_bool(const T& src) {
    if constexpr(std::is_convertible_v<T, bool>) {
      return static_cast<bool>(src);
    }
    return true;
  }

  static void print(const T& src, std::ostream& dst) { dst << src; }
};

template <>
struct Data_traits<json> {
  static constexpr bool is_sequence = false;
  static constexpr bool is_mapping = false;
  static constexpr bool is_value = true;

  static bool as_bool(const json& src) {
    if(src.is_boolean()) {
      return src.get<bool>();
    }

    if(src.is_null()) {
      return false;
    }
    return true;
  }

  static void print(const json& src, std::ostream& dst) { 
    if(src.is_string()) {
      dst << src.get<std::string>();
    }
    else {
      dst << src;
    }
  }
};

template <>
struct Data_traits<std::nullptr_t> {
  static constexpr bool is_sequence = false;
  static constexpr bool is_mapping = false;
  static constexpr bool is_value = false;

  static bool as_bool(const json& src) {
    return false;
  }
};

template <typename T>
struct Data_traits<std::vector<T>> {
  static constexpr bool is_sequence = true;
  static constexpr bool is_mapping = false;
  static constexpr bool is_value = false;

  static bool as_bool(const json& src) {
    return true;
  }
};

template <typename K, typename T>
struct Data_traits<
    std::map<K, T>,
    std::enable_if_t<std::is_constructible_v<std::string_view, K>>> {
  static constexpr bool is_sequence = false;
  static constexpr bool is_mapping = true;
  static constexpr bool is_value = false;

  static const T* lookup(const std::map<K, T>& src, std::string_view key) {
    auto found = src.find(K(key));
    if (found == src.end()) {
      return nullptr;
    }

    return &found->second;
  }

  static bool as_bool(const json& src) {
    return true;
  }
};

template <typename K, typename T>
struct Data_traits<
    std::unordered_map<K, T>,
    std::enable_if_t<std::is_constructible_v<std::string_view, K>>> {
  static constexpr bool is_sequence = false;
  static constexpr bool is_mapping = true;
  static constexpr bool is_value = false;

  static const T* lookup(const std::unordered_map<K, T>& src,
                         std::string_view key) {
    auto found = src.find(K(key));
    if (found == src.end()) {
      return nullptr;
    }

    return &found->second;
  }

  static bool as_bool(const json& src) {
    return true;
  }
};

class Data_provider {
 public:
  class Impl {
   public:
    virtual void get(const void*, std::ostream&) const {}
    virtual void visit(const void*,
                       std::function<void(Data_provider)> cb) const {}
    virtual Data_provider lookup(const void*, std::string_view key) const {
      return Data_provider{nullptr, nullptr};
    }

    virtual bool as_bool(const void*) const {return true;}
   protected:
    ~Impl() = default;
  };

  Data_provider() = default;
  Data_provider(const void* data, Impl* impl) : data_(data), impl_(impl) {}
  Data_provider(const Data_provider&) = default;
  Data_provider& operator=(const Data_provider&) = default;

  void get(std::ostream& dst) const { return impl_->get(data_, dst); }

  void visit(std::function<void(Data_provider)> cb) const {
    impl_->visit(data_, std::move(cb));
  }

  Data_provider lookup(std::string_view key) const {
    return impl_->lookup(data_, key);
  }

  bool as_bool() const {
    return impl_->as_bool(data_);
  }
 private:
  const void* data_ = nullptr;
  const Impl* impl_ = nullptr;
};

template <typename T>
Data_provider data_provider(const T& v);

template<typename T>
class Basic_provider_impl : public Data_provider::Impl {
public:
  virtual bool as_bool(const void* data) const {
    const T* as_t = reinterpret_cast<const T*>(data);
    return Data_traits<T>::as_bool(*as_t);
  }
};

template <typename T, typename ParentT = Basic_provider_impl<T>>
class Value_provider_impl : public ParentT {
 public:
  void get(const void* data, std::ostream& dst) const override {
    const T* as_t = reinterpret_cast<const T*>(data);
    Data_traits<T>::print(*as_t, dst);
  }
};

template <typename T, typename ParentT = Basic_provider_impl<T>>
class Sequence_provider_impl : public ParentT {
 public:
  void visit(const void* data,
             std::function<void(Data_provider)> cb) const override {
    const T* as_t = reinterpret_cast<const T*>(data);

    for (const auto& i : *as_t) {
      cb(data_provider(i));
    }
  }
};

template <typename T, typename ParentT = Basic_provider_impl<T>>
class Mapping_provider_impl : public ParentT {
 public:
  Data_provider lookup(const void* data, std::string_view key) const override {
    const T* as_t = reinterpret_cast<const T*>(data);

    return data_provider(*Data_traits<T>::lookup(*as_t, key));
  }
};

template <typename T, typename Enable = void>
struct get_provider_impl_t {
  using traits = Data_traits<T>;

  using with_value =
      std::conditional_t<traits::is_value, Value_provider_impl<T>,
                         Basic_provider_impl<T>>;

  using with_sequence =
      std::conditional_t<traits::is_sequence,
                         Sequence_provider_impl<T, with_value>, with_value>;

  using with_mapping =
      std::conditional_t<traits::is_mapping,
                         Mapping_provider_impl<T, with_sequence>,
                         with_sequence>;

  using type = with_mapping;
};

template <typename T>
Data_provider data_provider(const T& v) {
  using provider_t = typename get_provider_impl_t<T>::type;
  static provider_t provider;
  return Data_provider(&v, &provider);
}

}  // namespace tmpl
}  // namespace text
}  // namespace slt

#endif