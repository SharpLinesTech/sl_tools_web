#ifndef SLT_TEXT_TEMPLATE_OP_H
#define SLT_TEXT_TEMPLATE_OP_H


#include <iostream>
#include <memory>
#include <vector>

#include "slt/concur/future.h"
#include "slt/text/template/ast.h"

namespace slt {
namespace text {
  class Template;

namespace tmpl {
class Context_api;

class Op {
 public:
  // Renders the template to the destination stream
  virtual void render(std::ostream& dst, const Context_api& ctx) const = 0;
};

class Print_raw_op : public Op {
 public:
  Print_raw_op(std::string_view data);
  void render(std::ostream& dst, const Context_api& ctx) const override;

 private:
  std::string_view data_;
};

class Print_op : public Op {
 public:
  Print_op(std::unique_ptr<Expr> expr);
  void render(std::ostream& dst, const Context_api& ctx) const override;

 private:
  std::unique_ptr<Expr> expr_;
};

class For_op : public Op {
 public:
  For_op(std::string_view var_name, std::unique_ptr<Expr> expr,
         std::vector<std::unique_ptr<Op>> ops);
  void render(std::ostream& dst, const Context_api& ctx) const override;

 private:
  std::string_view var_name_;
  std::unique_ptr<Expr> expr_;
  std::vector<std::unique_ptr<Op>> ops_;
};

class Block_op : public Op {
 public:
  Block_op(std::string name, std::vector<std::unique_ptr<Op>> ops);
  void render(std::ostream& dst, const Context_api& ctx) const override;

  std::string name_;
  std::vector<std::unique_ptr<Op>> ops_;
};

class If_op : public Op {
 public:
  If_op(std::unique_ptr<Expr> expr, std::vector<std::unique_ptr<Op>> ops);
  void render(std::ostream& dst, const Context_api& ctx) const override;

  std::unique_ptr<Expr> expr_;
  std::vector<std::unique_ptr<Op>> ops_;
};

class Extend_op : public Op {
 public:
  Extend_op(std::vector<std::unique_ptr<Block_op>> ops);
  void render(std::ostream& dst, const Context_api& ctx) const override;

  void set_parent(const Template& tmpl);

  const Block_op* lookup_block(const std::string& name) const;
 private:
  const Template* template_;
  std::unordered_map<std::string, std::unique_ptr<Block_op>> blocks_;
};
}  // namespace tmpl
}  // namespace text
}  // namespace slt
#endif