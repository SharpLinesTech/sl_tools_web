#ifndef SLT_URI_H
#define SLT_URI_H

#include <cstdint>
#include <string_view>
#include "uriparser/Uri.h"

namespace slt {
class Uri {
  Uri(const Uri&) = delete;
  Uri& operator=(const Uri&) = delete;
public:
  Uri();
  Uri(Uri&&);
  Uri& operator=(Uri&&);
  
  Uri(std::string_view raw_);
  ~Uri();

  std::string_view scheme() const;
  std::string_view host() const;
  std::string path() const;
  std::uint16_t port() const;

 private:
  UriUriA data_;
  bool valid;
};
}  // namespace slt
#endif