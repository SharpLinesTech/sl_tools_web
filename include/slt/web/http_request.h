#ifndef SLT_WEB_HTTP_REQUEST_H
#define SLT_WEB_HTTP_REQUEST_H


#include "slt/uri.h"

#include <string>
#include <vector>
#include <optional>

namespace slt {

enum class Http_method {
  GET,
};

struct Http_request {
 public:
  Http_request(Http_method m, std::string uri, int v_ma, int v_mi, std::vector<std::pair<std::string, std::string>> h)
    : method(m)
    , raw_uri(std::move(uri))
    , uri(raw_uri)
    , http_version_major(v_ma)
    , http_version_minor(v_mi)
    , headers(std::move(h)) {}

  Http_request(const Http_request&) = delete;
  Http_request(Http_request&&) = default;
  Http_request& operator=(const Http_request&) = delete;
  Http_request& operator=(Http_request&&) = default;

  Http_method method;
  std::string raw_uri;
  Uri         uri;

  int http_version_major;
  int http_version_minor;
  std::vector<std::pair<std::string, std::string>> headers;
};

class Http_request_parser {
  int raw_http_version_major;
  int raw_http_version_minor;
  std::vector<std::pair<std::string, std::string>> raw_headers;

  std::string raw_uri;
  std::string raw_method;

 public:
  Http_request_parser();

  template <typename InputIterator>
  std::optional<Http_request> parse(InputIterator begin, InputIterator end) {
    while (begin != end) {
      bool done = consume(*begin++);
      if(done) {
        return Http_request{
          Http_method::GET,
          std::move(raw_uri),
          raw_http_version_major, 
          raw_http_version_minor, 
          std::move(raw_headers)};
      }
    }
    return std::nullopt;
  }

 private:
  /// Handle the next character of input.
  bool consume(char input);

  /// The current state of the parser.
  enum state {
    method_start,
    method,
    uri,
    http_version_h,
    http_version_t_1,
    http_version_t_2,
    http_version_p,
    http_version_slash,
    http_version_major_start,
    http_version_major,
    http_version_minor_start,
    http_version_minor,
    expecting_newline_1,
    header_line_start,
    header_lws,
    header_name,
    space_before_header_value,
    header_value,
    expecting_newline_2,
    expecting_newline_3
  } state_;
};
}  // namespace slt
#endif