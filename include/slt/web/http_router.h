#ifndef SLT_WEB_HTTP_ROUTER_H
#define SLT_WEB_HTTP_ROUTER_H

#include "slt/web/http_request.h"
#include "slt/web/http_reply.h"
#include "slt/web/http_server.h"
#include "slt/log.h"

#include <regex>
#include <vector>
#include <regex>

namespace slt {

  class Http_router {
  public:

    Http_reply operator()(const Http_request& req, Http_connection& conn) const;
    void add_route(Http_method method, std::regex pattern, Http_server::request_handler handler);

  private:

    struct Route {
      std::regex pattern;
      Http_server::request_handler handler;
    };

    std::map<Http_method, std::vector<Route>> routes_;
  };

}  // namespace slt
#endif