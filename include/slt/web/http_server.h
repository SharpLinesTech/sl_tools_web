#ifndef SLT_WEB_HTTP_SERVER_H
#define SLT_WEB_HTTP_SERVER_H

#define ASIO_STANDALONE
#include <asio.hpp>

#include "slt/web/http_request.h"
#include "slt/web/http_reply.h"

#include "slt/uri.h"

#include <map>
#include <memory>
#include <set>
#include <tuple>

namespace slt {

class Http_server;
class Http_connection : public std::enable_shared_from_this<Http_connection> {
public:
  Http_connection(asio::ip::tcp::socket socket, Http_server* parent);
  ~Http_connection();

private:
  void send(std::string_view data);
  void begin();
  void do_read();

  asio::ip::tcp::socket socket_;
  Http_server* parent_;

  std::unique_ptr<Http_request_parser> req_parser_;

  // Buffer for incoming data.
  std::array<char, 4096> recv_buffer_;

  friend class Http_server;
};


// This is a simple straightforward http server not meant for anything 
// particularly high-performance.
class Http_server {
  Http_server(const Http_server&) = delete;
  Http_server& operator=(const Http_server&) = delete;
 public:
  using request_handler = std::function<Http_reply(const Http_request&, Http_connection&)>;

  Http_server(std::string_view address, std::uint16_t port,
              asio::io_context& io_context, request_handler handler);

  ~Http_server();
 private:
  void do_accept();
  void remove_connection(Http_connection*);

  request_handler handler_;
  asio::io_context& io_context_;
  asio::ip::tcp::acceptor acceptor_;

  std::set<Http_connection*> connections_;
  
  friend class Http_connection;
};
}  // namespace slt
#endif