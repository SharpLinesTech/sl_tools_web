#include "slt/text/html.h"

#include "gumbo.h"

namespace slt {
  namespace {/*
    void output_cleaned(GumboNode* node, std::ostream& stream);
    void output_cleaned_contents(GumboNode* node, std::ostream& stream);
    void output_doctype(GumboNode* node, std::ostream& stream);

    void output_cleaned(GumboNode* node, std::ostream& stream) {
      switch(node->type) {
        case GUMBO_NODE_DOCUMENT:
          output_doctype(node, stream);
          output_cleaned_contents(node, stream);
        break;
        case GUMBO_NODE_TEXT:
          stream << node->v.text.text;
          break;
        
        case GUMBO_NODE_COMMENT:
          break;
      }
    }

    void output_doctype(GumboNode* node, std::ostream& stream) {
    if (node->v.document.has_doctype) {
        stream << "<!DOCTYPE "
          << node->v.document.name
          << ">\n";
      }
    }
    */
  }

  void minimize_html(std::string_view raw, std::ostream& stream) {
    stream << raw;
  }
}