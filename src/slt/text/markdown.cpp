#include "slt/text/markdown.h"
#include "slt/scope_guard.h"

#include "cmark.h"

namespace slt {
  void cmark_to_html(std::string_view raw, int options, std::ostream& stream) {

    // This is certainly not optimal, but it'll work...
    auto data = cmark_markdown_to_html(raw.data(), raw.size(), options);    
    auto del_data = on_scope_exit([data]() {
      free(data);
    });

    stream << data;
  }
}