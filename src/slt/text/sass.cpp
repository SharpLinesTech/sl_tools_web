#include "slt/text/sass.h"
#include "slt/scope_guard.h"

#include <vector>
#include "sass.h"

namespace slt {
  void sass_to_css(std::string_view raw, std::ostream& stream) {
    auto options = sass_make_options();
    sass_option_set_output_style(options, SASS_STYLE_COMPRESSED);
    sass_option_set_precision(options, 10);

    char* null_terminated_sass = reinterpret_cast<char*>(sass_alloc_memory(raw.length() + 1));
    std::memcpy(null_terminated_sass, raw.data(), raw.length());
    null_terminated_sass[raw.length()] = '\0';

    auto ctx = sass_make_data_context(null_terminated_sass);


    struct Sass_Context* ctx_out = sass_data_context_get_context(ctx);
    sass_data_context_set_options(ctx, options);
    sass_compile_data_context(ctx);


    stream << sass_context_get_output_string(ctx_out);

    sass_delete_data_context(ctx);
    sass_delete_options(options);
  }
}
