#include "slt/text/template.h"
#include "slt/log.h"
#include "slt/text/template/lex.h"

#include <algorithm>
#include <functional>

namespace slt {
namespace text {
namespace tmpl {
namespace {

using Expr_ptr = std::unique_ptr<Expr>;


struct Parser : public std::enable_shared_from_this<Parser> {
  
  Data_block data_;
  Template_lookup_fn lookup_fn_;

  Promise<Template> promise_;
  std::vector<std::unique_ptr<tmpl::Op>> ops_;
  std::mutex mutex_;
  int pending_dependencies = 0;

  Parser(Data_block data, Template_lookup_fn lookup_fn) 
    : data_(std::move(data))
    , lookup_fn_(std::move(lookup_fn)) {}

  Expr_ptr parse_primary(Token_iterator &b, Token_iterator e) {
    Expr_ptr result;
    switch (b->type) {
    case Token_t::string_literal:
      result = std::make_unique<String_literal>(b->raw);
      ++b;
      break;
    case Token_t::identifier:
      result = std::make_unique<Id_ref>(b->raw);
      ++b;
      break;
    case Token_t::number:
      break;
    case Token_t::parens_open:
      result = parse_parens(b, e);
      break;
    default:
      break;
    }
    return result;
  }

  Expr_ptr parse_parens(Token_iterator &b, Token_iterator e) {
    ++b;
    auto result = parse_expr(b, e);
    if (b->type != Token_t::parens_close) {
      throw std::runtime_error("expecting )");
    }
    ++b;
    return result;
  }

  Expr_ptr parse_expr(std::string_view raw) {
    Token_iterator b(raw);

    return parse_expr(b, Token_iterator());
  }

  Expr_ptr parse_expr(Token_iterator &b, Token_iterator e) {
    if (b == e) {
      return nullptr;
    }

    auto result = parse_primary(b, e);

    while (b != e) {
      switch (b->type) {
      case Token_t::period:
        ++b;
        if (b == e || b->type != Token_t::identifier) {
          throw std::runtime_error("expecting identifier");
        }
        result = std::make_unique<Resolve>(std::move(result), b->raw);
        ++b;
        break;
      default:
        throw std::runtime_error("extremely confused");
      }
    }

    return result;
  }

  std::unique_ptr<Op> handle_for_directive(Token_iterator &tag_ite,
                                           Token_iterator tag_end,
                                           std::string_view::iterator &ite,
                                           std::string_view::iterator end) {
    if (tag_ite == tag_end || tag_ite->type != Token_t::identifier) {
      throw std::runtime_error("expecting identifier");
    }

    auto iter_name = tag_ite->raw;
    ++tag_ite;

    if (tag_ite == tag_end || tag_ite->raw != "in") {
      throw std::runtime_error("expecting 'in'");
    }
    ++tag_ite;

    auto iter_src = parse_expr(tag_ite, tag_end);

    if (!iter_src) {
      throw std::runtime_error("expecting expression");
    }

    if (tag_ite != tag_end) {
      throw std::runtime_error("expecting end of tag");
    }

    return std::make_unique<For_op>(iter_name, std::move(iter_src),
                                    parse(ite, end, "endfor"));
  }

  std::unique_ptr<Block_op>
  handle_block_directive(Token_iterator &tag_ite, Token_iterator tag_end,
                         std::string_view::iterator &ite,
                         std::string_view::iterator end) {
    auto name_expr = parse_expr(tag_ite, tag_end);
    String_literal* name_as_literal = dynamic_cast<String_literal*>(name_expr.get());

    if(!name_as_literal) {
      throw std::runtime_error("block param must be string literal");
    }

    auto ops = parse(ite, end, "endblock");
    return std::make_unique<Block_op>(name_as_literal->data_, std::move(ops));
  }

  std::unique_ptr<Op> handle_extend_directive(Token_iterator &tag_begin,
                                              Token_iterator tag_end,
                                              std::string_view::iterator &ite,
                                              std::string_view::iterator end) {

    auto name_expr = parse_expr(tag_begin, tag_end);
    String_literal* name_as_literal = dynamic_cast<String_literal*>(name_expr.get());

    if(!name_as_literal) {
      throw std::runtime_error("extend param must be string literal");
    }

    auto parent_tmpl = lookup_fn_(name_as_literal->data_);

    bool in_block = false;

    std::vector<std::unique_ptr<Block_op>> blocks;
    while (true) {
      const std::string_view tag_start_str = "{%";
      const std::string_view tag_end_str = "%}";
      // Advance to the next open brace
      ite = std::search(ite, end,
                        std::boyer_moore_searcher(tag_start_str.begin(),
                                                  tag_start_str.end()));

      if (ite == end) {
        throw std::runtime_error("unterminated extend");
      }

      std::advance(ite, 2);
      auto sub_tag_end = std::search(
          ite, end,
          std::boyer_moore_searcher(tag_end_str.begin(), tag_end_str.end()));

      if (sub_tag_end == end) {
        throw std::runtime_error("unterminated tag");
      }

      std::string_view directive_string(&*ite, sub_tag_end - ite);
      ite = std::next(sub_tag_end, 2);

      Token_iterator b(directive_string);
      Token_iterator e;
      if (b->type != Token_t::identifier) {
        throw std::runtime_error("expecting identifier");
      }

      if (b->raw == "block") {
        ++b;
        blocks.push_back(handle_block_directive(b, e, ite, end));
      } else if (b->raw == "endextend") {
        break;
      } else {
        throw std::runtime_error("unexpected directive within extend block");
      }
    }

    auto result = std::make_unique<Extend_op>( std::move(blocks));
    auto self = shared_from_this();
    auto result_ptr = result.get();

    std::lock_guard l(self->mutex_);
    ++pending_dependencies;

    parent_tmpl.then([this, self, result_ptr](const Template& tmpl) {
      result_ptr->set_parent(tmpl);

      std::lock_guard l(mutex_);
      --pending_dependencies;
      if(pending_dependencies == 0 ) {
        finish();
      }
    });
    return result; 
  }

  std::unique_ptr<Op> handle_if_directive(Token_iterator &tag_begin,
                                          Token_iterator tag_end,
                                          std::string_view::iterator &ite,
                                          std::string_view::iterator end) {
    auto condition_expr = parse_expr(tag_begin, tag_end);

    auto ops = parse(ite, end, "endif");
    return std::make_unique<If_op>(move(condition_expr), move(ops));
  }

  std::unique_ptr<Op> handle_print(std::string_view::iterator &ite,
                                   std::string_view::iterator end) {
    const std::string_view tag_end_str = "}}";
    std::advance(ite, 2);
    auto tag_end = std::search(
        ite, end,
        std::boyer_moore_searcher(tag_end_str.begin(), tag_end_str.end()));

    if (tag_end == end) {
      throw std::runtime_error("unterminated tag");
    }

    std::string_view expr_string(&*ite, tag_end - ite);
    ite = std::next(tag_end, 2);

    return std::make_unique<Print_op>(parse_expr(expr_string));
  }

  std::tuple<bool, std::unique_ptr<Op>>
  handle_directive(std::string_view::iterator &ite,
                   std::string_view::iterator end,
                   std::string_view end_directive) {
    const std::string_view tag_end_str = "%}";
    std::advance(ite, 2);
    auto tag_end = std::search(
        ite, end,
        std::boyer_moore_searcher(tag_end_str.begin(), tag_end_str.end()));

    if (tag_end == end) {
      throw std::runtime_error("unterminated tag");
    }

    std::string_view directive_string(&*ite, tag_end - ite);
    ite = std::next(tag_end, 2);

    Token_iterator b(directive_string);
    Token_iterator e;

    if (b->type != Token_t::identifier) {
      throw std::runtime_error("expecting identifier");
    }

    if (b->raw == "extend") {
      ++b;
      return std::make_tuple(false, handle_extend_directive(b, e, ite, end));
    }
    if (b->raw == "block") {
      ++b;
      return std::make_tuple(false, handle_block_directive(b, e, ite, end));
    }
    if (b->raw == "for") {
      ++b;
      return std::make_tuple(false, handle_for_directive(b, e, ite, end));
    } else if (b->raw == "if") {
      ++b;
      return std::make_tuple(false, handle_if_directive(b, e, ite, end));
    } else if (!end_directive.empty() && b->raw == end_directive) {
      return std::make_tuple(true, nullptr);
    }
    throw std::runtime_error("unknown directive");
  }

  slt::Future<Template> parse() {
    std::string_view as_string(reinterpret_cast<char *>(data_.data()),
                             data_.size());
    auto ite = as_string.begin();
    auto end = as_string.end();

    pending_dependencies = 1;
    
    ops_ = parse(ite, end, {});

    auto result = promise_.get_future();

    std::lock_guard l(mutex_);
    pending_dependencies--;
    if(pending_dependencies == 0) {
      finish();
    }

    return result;
  }

  void finish() {
    promise_.set_value(Template{std::move(data_), std::move(ops_)});
  }

  std::vector<std::unique_ptr<Op>> parse(std::string_view::iterator &ite,
                                         std::string_view::iterator end,
                                         std::string_view end_directive) {
    using Ite = std::string_view::iterator;

    std::vector<std::unique_ptr<Op>> result;

    auto raw_start = ite;
    auto consume_raw = [&](Ite raw_end) {
      if (raw_end != raw_start) {
        result.emplace_back(std::make_unique<Print_raw_op>(
            std::string_view(&*raw_start, raw_end - raw_start)));
        raw_start = raw_end;
      }
    };

    while (true) {
      // Advance to the next open brace
      ite = std::find(ite, end, '{');

      if (ite == end)
        break;

      auto next = std::next(ite);
      // Handle the last character being an open brace
      if (ite == end)
        break;

      switch (*next) {
      case '{':
        consume_raw(ite);
        result.emplace_back(handle_print(ite, end));
        raw_start = ite;
        break;
      case '%': {
        consume_raw(ite);
        auto [bail, act] = handle_directive(ite, end, end_directive);
        raw_start = ite;

        if (bail) {
          return result;
        }

        result.emplace_back(std::move(act));
      } break;
      default:
        ite = std::next(next);
        break;
      }
    }

    consume_raw(ite);
    return result;
  }
};
} // namespace
} // namespace tmpl


slt::Future<Template> load_template(slt::Data_block raw_data, Template_lookup_fn fn) {
  auto p = std::make_shared<slt::text::tmpl::Parser>(
    std::move(raw_data), 
    std::move(fn)
  );

  return p->parse();
}

Template::Template(slt::Data_block data, std::vector<std::unique_ptr<tmpl::Op>> ops)
    : raw_data(std::move(data))
    , operations_(std::move(ops)) {}

Template::~Template() {}

// Renders the template to the destination stream
void Template::render(std::ostream &dst, const tmpl::Context_api &ctx) const {
  for (const auto &op : operations_) {
    op->render(dst, ctx);
  }
}

} // namespace text
} // namespace slt
