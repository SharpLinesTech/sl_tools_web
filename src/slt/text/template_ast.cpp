#include "slt/text/template/ast.h"
#include "slt/text/template/context.h"

#include <sstream>

namespace {
std::string decode_string_literal(std::string_view raw) {
  // remove the quotes around it
  raw = raw.substr(1, raw.size() - 2);

  return std::string(raw);
}
}  // namespace
namespace slt {
namespace text {
namespace tmpl {
  String_literal::String_literal(std::string_view raw_data)
      : data_(decode_string_literal(raw_data)) {}

  Data_provider String_literal::eval(const Context_api&) const {
    return data_provider(data_);
  }

  Id_ref::Id_ref(std::string_view init_id) : id_(init_id) {}

  Data_provider Id_ref::eval(const Context_api& ctx) const {
    auto found = ctx.lookup(id_);
    if (!found) {
      return data_provider(nullptr);
    }

    return *found;
  }

  Resolve::Resolve(std::unique_ptr<Expr> lhs, std::string_view init_id)
      : lhs_(std::move(lhs)), key_(init_id) {}

  Data_provider Resolve::eval(const Context_api& ctx) const {
    return lhs_->eval(ctx).lookup(key_);
  }

  Filter::Filter(std::unique_ptr<Expr> lhs, std::string_view name)
      : lhs_(std::move(lhs)), filter_name_(name) {}

  Data_provider Filter::eval(const Context_api& ctx) const {
  //  auto& filter = ctx.lookup_filter(filter_name_);

//    return lhs_->eval(ctx).lookup(key_);

    return Data_provider();

  }
}  // namespace template
}  // namespace text
}  // namespace slt
