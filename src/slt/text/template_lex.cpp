#include "slt/text/template/lex.h"

#include <cassert>
#include <iostream>
#include <algorithm>
#include <optional>


namespace slt {
namespace text {
namespace tmpl {
namespace {


constexpr const char* white_space = " \t\n\r\f\v\0";

// String trimming
constexpr std::string_view ltrim(std::string_view str) {
  auto found = str.find_first_not_of(white_space);
  return found == std::string_view::npos ? std::string_view{}
                                         : str.substr(found);
}

constexpr std::string_view rtrim(std::string_view str) {
  return str.substr(0, str.find_last_not_of(white_space) + 1);
}

constexpr std::string_view trim(std::string_view str) {
  return ltrim(rtrim(str));
}

// Returns the lenghts of the string that matches the predicate.
template <typename Unary>
std::string_view get_prefix(std::string_view data, Unary predicate) {
  auto end = std::find_if_not(data.begin(), data.end(), predicate);

  return {data.data(), std::size_t(end - data.begin())};
}


bool is_alpha(char c) {
  return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}

bool is_id_char(char c) { return is_alpha(c) || c == '_'; }

bool is_num(char c) { return (c >= '0' && c <= '9'); }
}  // namespace

Token get_head_token(std::string_view data) {
  assert(data.length() > 0);
  assert(data.find_first_of(white_space) != 0);

  auto try_tok = [&](std::string_view tok,
                     Token_t type) -> std::optional<Token> {
    if (data.length() >= tok.length() &&
        data.compare(0, tok.length(), tok) == 0) {
      return Token{type, data.substr(0, tok.length())};
    }
    return std::nullopt;
  };

  // suuuuuuuper lazy, we could use a trie instead, but we'll deal with this
  // if performance ever becomes an issue.
  if (auto t = try_tok("==", Token_t::eq_cmp)) return *t;
  if (auto t = try_tok("<=", Token_t::le_cmp)) return *t;
  if (auto t = try_tok(">=", Token_t::ge_cmp)) return *t;
  if (auto t = try_tok("!=", Token_t::neq_cmp)) return *t;

  if (auto t = try_tok("<", Token_t::lt_cmp)) return *t;
  if (auto t = try_tok(">", Token_t::gt_cmp)) return *t;

  if (auto t = try_tok("=", Token_t::eq)) return *t;
  if (auto t = try_tok(".", Token_t::period)) return *t;
  if (auto t = try_tok("(", Token_t::parens_open)) return *t;
  if (auto t = try_tok(")", Token_t::parens_close)) return *t;
  if (auto t = try_tok("-", Token_t::minus)) return *t;
  if (auto t = try_tok("+", Token_t::plus)) return *t;
  if (auto t = try_tok("*", Token_t::mul)) return *t;
  if (auto t = try_tok("/", Token_t::div)) return *t;

  if (data[0] == '"' || data[0] == '\'') {
    auto delim_end = data[0];
    std::size_t len = 0;
    auto ptr = &data[1];
    while (*ptr != delim_end) {
      ++ptr;
      ++len;
      if (len + 1 > data.length()) {
        throw std::runtime_error("untermined string literal");
      }
    }

    return {Token_t::string_literal, data.substr(0, len + 2)};
  }

  if (is_id_char(data[0])) {
    return {Token_t::identifier, get_prefix(data, is_id_char)};
  }
  if (is_num(data[0])) {
    return {Token_t::number, get_prefix(data, is_num)};
  }

  throw std::runtime_error("tokenizer failure");
}

Token_iterator::Token_iterator(std::string_view data) : data_(data) {
  data_ = trim(data_);
  if (data_.length() == 0) {
    data_ = std::string_view();
  } else {
    token_ = get_head_token(data_);
  }
}

bool Token_iterator::operator==(const Token_iterator& rhs) const {
  return (data_.data() == rhs.data_.data()) &&
         (data_.length() == rhs.data_.length());
}

bool Token_iterator::operator!=(const Token_iterator& rhs) const {
  return !((*this) == rhs);
}

Token_iterator& Token_iterator::operator++() {
  *this = Token_iterator(data_.substr(token_.raw.length()));
  return *this;
}
}  // namespace tmpl
}  // namespace text
}  // namespace slt
