#include "slt/text/template.h"
#include "slt/text/template/op.h"
#include "slt/text/template/context.h"

#include <string_view>
#include <iostream>

namespace slt {
namespace text {
namespace tmpl {
  Print_raw_op::Print_raw_op(std::string_view data)
    : data_(std::move(data)) {}

void Print_raw_op::render(std::ostream& dst, const Context_api& ctx) const {
  dst << data_;
}

Print_op::Print_op(std::unique_ptr<Expr> expr) : expr_(std::move(expr)) {}

void Print_op::render(std::ostream& dst, const Context_api& ctx) const {
  auto evaled = expr_->eval(ctx);

  evaled.get(dst);
}

class For_ctx : public Context_api {
 public:
  For_ctx(const Context_api& parent, std::string_view var_name)
      : parent_(parent), var_name_(var_name) {}

  std::optional<Data_provider> lookup(
      const std::string_view& name) const override {
    if (name == var_name_) {
      return data_;
    }
    return parent_.lookup(name);
  }

  const Context_api& parent_;
  std::string_view var_name_;
  Data_provider data_;
};

For_op::For_op(std::string_view var_name, std::unique_ptr<Expr> expr,
               std::vector<std::unique_ptr<Op>> ops)
    : var_name_(var_name), expr_(std::move(expr)), ops_(std::move(ops)) {}

void For_op::render(std::ostream& dst, const Context_api& ctx) const {
  auto list = expr_->eval(ctx);

  For_ctx sub_context(ctx, var_name_);

  list.visit([&](Data_provider d) {
    sub_context.data_ = d;
    for (const auto& i : ops_) {
      i->render(dst, sub_context);
    }
  });
}

If_op::If_op(std::unique_ptr<Expr> expr, std::vector<std::unique_ptr<Op>> ops)
  : expr_(move(expr))
  , ops_(move(ops)) {

}

void If_op::render(std::ostream& dst, const Context_api& ctx) const {
 if(expr_->eval(ctx).as_bool()) {
   for (const auto& i : ops_) {
     i->render(dst, ctx);
   }
  }
}

struct Extend_context : public Context_api {
  Extend_context(const Context_api& p, const Extend_op* tgt) : parent_(&p), tgt_(tgt) {}

  std::optional<Data_provider> lookup(
      const std::string_view& name) const override {
    return parent_->lookup(name);
  }

  const Block_op* lookup_block(const std::string_view& name) const override {
    auto result = tgt_->lookup_block(std::string(name));
    if(result) {
      return result;
    }
    return parent_->lookup_block(name);
  }

private:
  const Context_api* parent_;
  const Extend_op* tgt_;
};

Extend_op::Extend_op(std::vector<std::unique_ptr<Block_op>> ops) {

  for(auto& b : ops) {
    blocks_[b->name_] = std::move(b);    
  }

}

void Extend_op::set_parent(const Template& tmpl) {
  template_ = &tmpl;
}

void Extend_op::render(std::ostream& dst, const Context_api& ctx) const {
  Extend_context sub_ctx(ctx, this);

  template_->render(dst, sub_ctx);
}

const Block_op* Extend_op::lookup_block(const std::string& name) const {
  auto found = blocks_.find(name);
  if(found == blocks_.end()) {
    return nullptr;
  }
  return found->second.get();
}

Block_op::Block_op(std::string name, std::vector<std::unique_ptr<Op>> ops) 
: name_(std::move(name))
, ops_(std::move(ops)) {

}

void Block_op::render(std::ostream& dst, const Context_api& ctx) const {
  auto overwritten = ctx.lookup_block(name_);
  if(overwritten) {
    for (const auto& i : overwritten->ops_) {
      i->render(dst, ctx);
    }
  }
  else {
    for (const auto& i : ops_) {
      i->render(dst, ctx);
    }
  }
}

}  // namespace text
}  // namespace slt
}  // namespace cpl_tmpl