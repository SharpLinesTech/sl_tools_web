#include "slt/uri.h"

#include <istream>
#include <sstream>
#include <stdexcept>

#include "slt/mem/read_buf.h"
#include "slt/log.h"

namespace slt {

namespace {
std::string_view r_to_v(const UriTextRangeA& range) {
  return std::string_view(range.first, range.afterLast - range.first);
}
}  // namespace

Uri::Uri(std::string_view raw_) {
  slt::log->info("Building URI from: {}", raw_);
  auto start = raw_.data();
  auto end = start + raw_.length();
  auto status = uriParseSingleUriExA(&data_, start, end, nullptr);
  if (status != 0) {
    throw std::invalid_argument(fmt::format("failed to parse uri {}", status));
  }
  valid = true;
}

Uri::Uri() : valid(false) {
}
Uri::Uri(Uri&& rhs) {
  data_ = rhs.data_;
  valid = rhs.valid;
  rhs.valid = false;
}  

Uri& Uri::operator=(Uri&& rhs) {
  data_ = rhs.data_;
  valid = rhs.valid;
  rhs.valid = false;

  return *this;
}


std::string_view Uri::scheme() const { return r_to_v(data_.scheme); }

std::string_view Uri::host() const { return r_to_v(data_.hostText); }

std::uint16_t Uri::port() const {
  std::uint16_t result = 0;

  Read_buf buf(r_to_v(data_.portText));
  // TODO: That temporary std::string is a shame.
  std::istream(&buf) >> result;

  return result;
}

std::string Uri::path() const {
  return "";
}

Uri::~Uri() { if(valid) {uriFreeUriMembersA(&data_); }}
}  // namespace slt