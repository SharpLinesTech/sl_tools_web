#include "slt/web/http_request.h"

#include "slt/log.h"

#include <string>

namespace slt {


namespace {
  bool is_char(int c) { return c >= 0 && c <= 127; }

  bool is_ctl(int c) {
    return (c >= 0 && c <= 31) || (c == 127);
  }

  bool is_tspecial(int c) {
    switch (c) {
      case '(':
      case ')':
      case '<':
      case '>':
      case '@':
      case ',':
      case ';':
      case ':':
      case '\\':
      case '"':
      case '/':
      case '[':
      case ']':
      case '?':
      case '=':
      case '{':
      case '}':
      case ' ':
      case '\t':
        return true;
      default:
        return false;
    }
  }

  bool is_digit(int c) { return c >= '0' && c <= '9'; }
}

Http_request_parser::Http_request_parser() : state_(method_start) {}

bool Http_request_parser::consume(char input) {
  switch (state_) {
    case method_start:
      if (!is_char(input) || is_ctl(input) || is_tspecial(input)) {
        throw std::runtime_error("");
      } else {
        state_ = method;
        raw_method.push_back(input);
        return false;
      }
    case method:
      if (input == ' ') {
        state_ = uri;
        return false;
      } else if (!is_char(input) || is_ctl(input) || is_tspecial(input)) {
        throw std::runtime_error("");
      } else {
        raw_method.push_back(input);
        return false;
      }
    case uri:
      if (input == ' ') {
        state_ = http_version_h;
        return false;
      } else if (is_ctl(input)) {
        throw std::runtime_error("");
      } else {
        raw_uri.push_back(input);
        return false;
      }
    case http_version_h:
      if (input == 'H') {
        state_ = http_version_t_1;
        return false;
      } else {
        throw std::runtime_error("");
      }
    case http_version_t_1:
      if (input == 'T') {
        state_ = http_version_t_2;
        return false;
      } else {
        throw std::runtime_error("");
      }
    case http_version_t_2:
      if (input == 'T') {
        state_ = http_version_p;
        return false;
      } else {
        throw std::runtime_error("");
      }
    case http_version_p:
      if (input == 'P') {
        state_ = http_version_slash;
        return false;
      } else {
        throw std::runtime_error("");
      }
    case http_version_slash:
      if (input == '/') {
        raw_http_version_major = 0;
        raw_http_version_minor = 0;
        state_ = http_version_major_start;
        return false;
      } else {
        throw std::runtime_error("");
      }
    case http_version_major_start:
      if (is_digit(input)) {
        raw_http_version_major = raw_http_version_major * 10 + input - '0';
        state_ = http_version_major;
        return false;
      } else {
        throw std::runtime_error("");
      }
    case http_version_major:
      if (input == '.') {
        state_ = http_version_minor_start;
        return false;
      } else if (is_digit(input)) {
        raw_http_version_major = raw_http_version_major * 10 + input - '0';
        return false;
      } else {
        throw std::runtime_error("");
      }
    case http_version_minor_start:
      if (is_digit(input)) {
        raw_http_version_minor = raw_http_version_minor * 10 + input - '0';
        state_ = http_version_minor;
        return false;
      } else {
        throw std::runtime_error("");
      }
    case http_version_minor:
      if (input == '\r') {
        state_ = expecting_newline_1;
        return false;
      } else if (is_digit(input)) {
        raw_http_version_minor = raw_http_version_minor * 10 + input - '0';
        return false;
      } else {
        throw std::runtime_error("");
      }
    case expecting_newline_1:
      if (input == '\n') {
        state_ = header_line_start;
        return false;
      } else {
        throw std::runtime_error("");
      }
    case header_line_start:
      if (input == '\r') {
        state_ = expecting_newline_3;
        return false;
      } else if (!raw_headers.empty() && (input == ' ' || input == '\t')) {
        state_ = header_lws;
        return false;
      } else if (!is_char(input) || is_ctl(input) || is_tspecial(input)) {
        throw std::runtime_error("");
      } else {
        raw_headers.push_back({});
        raw_headers.back().first.push_back(input);
        state_ = header_name;
        return false;
      }
    case header_lws:
      if (input == '\r') {
        state_ = expecting_newline_2;
        return false;
      } else if (input == ' ' || input == '\t') {
        return false;
      } else if (is_ctl(input)) {
        throw std::runtime_error("");
      } else {
        state_ = header_value;
        raw_headers.back().second.push_back(input);
        return false;
      }
    case header_name:
      if (input == ':') {
        state_ = space_before_header_value;
        return false;
      } else if (!is_char(input) || is_ctl(input) || is_tspecial(input)) {
        throw std::runtime_error("");
      } else {
        raw_headers.back().first.push_back(input);
        return false;
      }
    case space_before_header_value:
      if (input == ' ') {
        state_ = header_value;
        return false;
      } else {
        throw std::runtime_error("");
      }
    case header_value:
      if (input == '\r') {
        state_ = expecting_newline_2;
        return false;
      } else if (is_ctl(input)) {
        throw std::runtime_error("");
      } else {
        raw_headers.back().second.push_back(input);
        return false;
      }
    case expecting_newline_2:
      if (input == '\n') {
        state_ = header_line_start;
        return false;
      } else {
        throw std::runtime_error("");
      }
    case expecting_newline_3:
      if (input == '\n') return true;
      throw std::runtime_error("");;
    default:
      throw std::runtime_error("");
  }
}


}  // namespace slt