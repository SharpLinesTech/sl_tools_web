#include "slt/web/http_router.h"

#include "slt/log.h"

#include <string>

namespace slt {
  Http_reply Http_router::operator()(const slt::Http_request& req, slt::Http_connection& conn) const {
    auto patterns = routes_.find(req.method);
    if(patterns != routes_.end()) {

      auto path = req.raw_uri;
      for(const auto& p : patterns->second) {
        auto match = std::regex_match(path, p.pattern);
        if(match) {
          return p.handler(req, conn);
        }
      }
    }
    Http_reply _404_reply;
    return _404_reply;
  }
    
  void Http_router::add_route(Http_method method, std::regex pattern, Http_server::request_handler handler) {
    routes_[method].push_back({std::move(pattern), std::move(handler)});
  }
}  // namespace slt