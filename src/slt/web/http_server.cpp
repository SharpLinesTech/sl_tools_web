#include "slt/web/http_server.h"

#include "slt/log.h"

#include <string>

namespace slt {

Http_connection::Http_connection(asio::ip::tcp::socket socket,
                                 Http_server* parent)
  : socket_(std::move(socket)), parent_(parent) {
    slt::log->info("Connection comes alive: {}", (void*)this);
  }

void Http_connection::begin() { do_read(); }

Http_connection::~Http_connection() { slt::log->info("arrrrg"); parent_->remove_connection(this); slt::log->info("clean"); }

void Http_connection::do_read() {
  auto self(shared_from_this());

  socket_.async_read_some(
      asio::buffer(recv_buffer_),
      [this, self](std::error_code ec, std::size_t bytes_transferred) {
        if (!ec) {
          if (!req_parser_) {
            req_parser_ = std::make_unique<Http_request_parser>();
          }
          auto result =
              req_parser_->parse(recv_buffer_.data(),
                                 recv_buffer_.data() + bytes_transferred);

          if (result) {
            req_parser_.reset();
            parent_->handler_(*result, *this);

          } else {
            do_read();
          }
        }
      });
}

void Http_connection::send(std::string_view data) {
  auto self(shared_from_this());
  asio::async_write(
      socket_, asio::buffer(data),
      [this, self](std::error_code ec, std::size_t) { do_read(); });
}

Http_server::Http_server(std::string_view address, std::uint16_t port,
                         asio::io_context& io_context, request_handler handler)
    : handler_(std::move(handler)), io_context_(io_context), acceptor_(io_context) {
  asio::ip::tcp::resolver resolver(io_context_);
  asio::ip::tcp::endpoint endpoint =
      *resolver.resolve(address, std::to_string(port)).begin();
  acceptor_.open(endpoint.protocol());
  acceptor_.set_option(asio::ip::tcp::acceptor::reuse_address(true));
  acceptor_.bind(endpoint);
  acceptor_.listen();

  do_accept();
}

Http_server::~Http_server() {}

void Http_server::do_accept() {
  acceptor_.async_accept(
      [this](std::error_code ec, asio::ip::tcp::socket socket) {
        if (!acceptor_.is_open()) {
          return;
        }

        if (!ec) {
          slt::log->info("accepted connection");
          auto new_connection =
              std::make_shared<Http_connection>(std::move(socket), this);
          connections_.insert(new_connection.get());
          new_connection->begin();
        }
        do_accept();
      });
}


void Http_server::remove_connection(Http_connection* what) {
  slt::log->info("I am {}, and am deleting {} ... {}" , (void*)this, (void*)what, connections_.size());
  connections_.erase(what);
}

}  // namespace slt