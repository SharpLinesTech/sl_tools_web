add_executable(slt_site_builder main.cpp)
set_property(TARGET slt_site_builder PROPERTY FOLDER "tool" )

target_link_libraries(slt_site_builder slt_web)

install(TARGETS slt_site_builder 
        EXPORT sl_tools_webTargets
        DESTINATION bin)