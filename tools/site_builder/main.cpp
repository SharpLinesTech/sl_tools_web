#include "slt/concur/future.h"
#include "slt/concur/multi_future.h"
#include "slt/concur/work_pool.h"
#include "slt/core.h"
#include "slt/file/read.h"
#include "slt/file/write.h"
#include "slt/json.h"
#include "slt/log.h"
#include "slt/mem/data_view.h"
#include "slt/text/template.h"
#include "slt/text/markdown.h"
#include "slt/text/sass.h"
#include "slt/web/http_router.h"
#include "slt/web/http_server.h"

#include <filesystem>
#include <future>
#include <sstream>

namespace fs = std::filesystem;

slt::Setting<std::string>
    meta_path("meta", "meta.json",
              "path to the file containing site's meta information");

slt::Setting<fs::path> dst_path("output", "build",
                                "directory where to store the built site");

void serve(std::uint16_t port);

class Template_pool {
public:
  Template_pool() {
    lookup_fn_ = [this](std::string_view name) {
      return this->get(std::string(name));
    };
  }
  Template_pool(const Template_pool &) = delete;
  Template_pool(Template_pool &&) = default;
  Template_pool &operator=(const Template_pool &) = delete;
  Template_pool &operator=(Template_pool &&) = default;

  using res_t = slt::text::Template;

  slt::Shared_future<res_t> get(const std::string &name) {
    slt::log->info("template requested: {}", name);
    std::lock_guard l(mutex_);

    auto &entry = entries_[name];
    if (entry.valid()) {
      return entry;
    }

    entry = slt::file::async_read(name).then([this](slt::Data_block d) {
      return slt::text::load_template(std::move(d), lookup_fn_);
    });

    return entry;
  }

private:
  slt::text::Template_lookup_fn lookup_fn_;

  std::mutex mutex_;
  std::unordered_map<std::string, slt::Shared_future<res_t>> entries_;
};

bool is_pattern_path(std::string_view path) {
  return std::find(path.begin(), path.end(), '*') != path.end();
}

void generate_page(fs::path path, const slt::text::Template &tmpl,
                   const slt::text::tmpl::Context_api &data) {
  std::ostringstream result;
  tmpl.render(result, data);

  auto dst = dst_path.get() / path;
  create_directories(dst.parent_path());

  slt::file::write(result.str(), dst.string());
}

std::tuple<std::string_view, std::string_view> extract_front_matter(std::string_view txt) {
  const std::string_view delim = "---";
  const std::size_t delim_len = delim.length();

    auto delim_pos = txt.find(delim);

    std::string_view fm;
    std::string_view payload;

  if(delim_pos == std::string_view::npos) {
    payload = txt;
  } else {
    fm = std::string_view{txt.data(), delim_pos};
    auto payload_offset = delim_pos + delim_len;
    payload = std::string_view{txt.data() + payload_offset, txt.length() - payload_offset};
  }
  return {fm, payload};
}

void generate_page_from_src(slt::Const_data_view src, 
                            const slt::text::Template &path_tmpl,
                            const slt::text::Template &tmpl,
                            const slt::text::tmpl::Context_api &data) {
  auto [fm, txt] = extract_front_matter({src.data, src.size});

  slt::text::tmpl::Context sub_ctx(&data);


  std::ostringstream rendered_content;
  slt::cmark_to_html(txt, 0, rendered_content);
  auto content_str = rendered_content.str();
  sub_ctx.add_root("content", content_str);

  auto ws_chars = " \t\n\r";

  fm.remove_prefix(std::min(fm.find_first_not_of(ws_chars), fm.size()));
  fm.remove_suffix(fm.size() - std::min(fm.find_last_not_of(ws_chars)+1, fm.size()));


  slt::json fm_json;
  if(!fm.empty()) {
    fm_json = slt::json::parse(fm);

    for (const auto &d : fm_json.items()) {
      sub_ctx.add_root(d.key(), d.value());
    }
  }

  std::ostringstream path;
  path_tmpl.render(path, sub_ctx);

  std::ostringstream result;
  tmpl.render(result, sub_ctx);



  auto dst = dst_path.get() / path.str();
  create_directories(dst.parent_path());

  slt::file::write(result.str(), dst.string());
}

void build_stylesheets(const slt::json& meta, const slt::text::tmpl::Context& ctx) {
  for (auto &sheet : meta.at("stylesheets")) {
    auto src = sheet.at("src").get<std::string>();

    auto src_fut = slt::file::async_read(src);

    src_fut.then([&sheet](slt::Data_block d) {
      slt::log->info("ready for css compilation");

      auto dst = dst_path.get() / sheet.at("path").get<std::string>();
      create_directories(dst.parent_path());
      
      std::ostringstream sassed;
      slt::sass_to_css(std::string_view(d.data(), d.size()), sassed);
      slt::file::write(sassed.str(), dst.string());
    });
  }
}

int main(int argc, const char *argv[]) {
  slt::Core core(argc, argv);

  create_directories(dst_path.get());

  auto hw_threads = std::thread::hardware_concurrency();
  slt::log->info("found {} hardware threads", hw_threads);
  slt::concur::Work_pool work_pool(hw_threads, hw_threads);
  work_pool.make_current();

  Template_pool templates;

  
  // Load the site's meta data.
  // This should be the one and only synchronous load.
  auto meta = slt::json::parse(slt::file::read(meta_path.get()));

  
  
  slt::text::tmpl::Context root_context;
  auto meta_data = meta.find("data");
  if (meta_data != meta.end()) {
    for (const auto &d : meta_data->items()) {
      root_context.add_root(d.key(), d.value());
    }
  }

  build_stylesheets(meta, root_context);

  for (auto &page : meta.at("pages")) {

  
    // The root template used for rendering the page.
    auto tmpl_fut = templates.get(page.at("root"));

    auto render_ctx = std::make_shared<slt::text::tmpl::Context>(&root_context);
    auto page_data = page.find("data");
    if (page_data != page.end()) {
      for (const auto &d : page_data->items()) {
        render_ctx->add_root(d.key(), d.value());
      }
    }

    auto page_src = page.find("src");
    if (page_src != page.end()) {
      fs::path src_path = *page_src;

      const auto &path_str = page.at("path").get<std::string>();
      slt::Data_block path_data(path_str.begin(), path_str.end());
      auto path_tmpl_fut =
            shared(slt::text::load_template(std::move(path_data), nullptr));

      if (is_directory(src_path)) {
        for (auto &p : fs::recursive_directory_iterator(src_path)) {
          auto src_fut = slt::file::async_read(p.path().string());

          slt::tie_futures(path_tmpl_fut, tmpl_fut, std::move(src_fut))
              .then([render_ctx](auto path_tmpl, auto tmpl, auto src) {
                generate_page_from_src(src, path_tmpl.data(), tmpl.data(),
                                       *render_ctx);
              });
        }
      } else if (is_regular_file(src_path)) {
        auto src_fut = slt::file::async_read(src_path);

        slt::tie_futures(path_tmpl_fut, tmpl_fut, std::move(src_fut))
            .then([render_ctx](auto path_tmpl, auto tmpl, auto src) {
              generate_page_from_src(src, path_tmpl.data(), tmpl.data(),
                                     *render_ctx);
            });

      } else {
        throw std::runtime_error("bad src");
      }
    } else {
      tmpl_fut.then([render_ctx, &page](const slt::text::Template &tmpl) {
        generate_page(page.at("path"), tmpl, *render_ctx);
      });
    }
  }

std::cout << "start waiting...\n";
  work_pool.wait_idle();
  std::cout << "done waiting...\n";

  return 0;
}

void serve(std::uint16_t port) {
  asio::io_context network;
  slt::Http_router router;

  router.add_route(slt::Http_method::GET, std::regex("/allo"),
                   [](const auto &req, auto &conn) {
                     slt::Http_reply result;

                     return result;
                   });

  router.add_route(slt::Http_method::GET, std::regex("/cool"),
                   [](const auto &req, auto &conn) {
                     slt::Http_reply result;

                     return result;
                   });

  slt::Http_server server("localhost", port, network, std::ref(router));

  network.run();
}